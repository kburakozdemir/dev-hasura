#!/bin/bash

# Usage
# bash insert.sh "GRAPHQL_API_ENDPOINT" "GRAPHQL_ADMIN_SECRET"

function append_repo_data {

  DATA=$(curl --header "Authorization: token $6" --silent "https://api.github.com/repos/$2")

  SUBSTRING=$(echo $2 | cut -d'/' -f 1)
  TEMPJSON="./tmp/""$SUBSTRING"".json"

  echo "{\"repo_id\": $1,\"data\": $DATA}" >"$TEMPJSON"

  GRAPHQL_API_ENDPOINT="$4/v1/graphql"
  GRAPHQL_ADMIN_SECRET="$5"

  ./node_modules/.bin/gq \
    $GRAPHQL_API_ENDPOINT \
    -H "Content-Type: application/json" \
    -H "X-Hasura-Admin-Secret: $GRAPHQL_ADMIN_SECRET" \
    --queryFile="./query.gql" \
    --variablesFile="$TEMPJSON" \
    >>./tmp/log.txt
}

curl -s \
  -H "Content-Type: application/json" \
  -H "X-Hasura-Admin-Secret: $2" \
  "$1/api/rest/getRepoList" |
  jq -c '.repo[]' \
    >./tmp/libraries.txt

readarray -t libraries <./tmp/libraries.txt

IFS=$'\n' #read til newline
for library in ${libraries[@]}; do
  libraryId=$(jq '.id' <<<"${library}")
  libraryUrl=$(jq '.repo_url' <<<"${library}")
  libraryAlias="$libraryUrl"
  echo $libraryUrl
  echo "$libraryId has url $libraryUrl"
  append_repo_data $libraryId "$(sed -e 's/^"//' -e 's/"$//' <<<"$libraryUrl")" "$libraryAlias" $1 $2 $3
done
unset IFS
