# Hasura Notes

## Dependencies

```bash
npm ci
sudo apt install jq
```

## Versions

Node v16.16.0

## graphqurl

[hasura/graphqurl](https://github.com/hasura/graphqurl)

## SCSS

```bash
sass public/css/style.scss public/css/style.css
```
