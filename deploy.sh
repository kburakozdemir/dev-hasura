#!/bin/bash

FILE="insert_drupal_banvit.sh"
rsync -avzthi \
  --delete \
  ./drupal/$FILE \
  temporary-server-binbiriz:/home/binbiriz/gecici/hasura/$FILE

FILE="insert_drupal_hestia.sh"
rsync -avzthi \
  --delete \
  ./drupal/$FILE \
  stgm-hestia-binbiriz:/home/binbiriz/gecici/hasura/$FILE

FILE="insert_drupal_polyhymnia.sh"
rsync -avzthi \
  --delete \
  ./drupal/$FILE \
  hetzner-live-polyhymnia-binbiriz:/home/binbiriz/gecici/hasura/$FILE

FILE="insert_drupal_terpsichore.sh"
rsync -avzthi \
  --delete \
  ./drupal/$FILE \
  hetzner-test-binbiriz:/home/binbiriz/gecici/hasura/$FILE
