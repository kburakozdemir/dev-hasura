BEGIN {
  printf "{"
  printf "\"Server\": \"" machine "\","
  printf "\"Location\": \"" location "\","
  printf "\"FileSystems\":["
}
{
  if ($1 != "Filesystem") {
    if (i) {
      printf ","
      }
      printf "{\"mount\":\"" $6 "\",\"size\":\"" $2 "\",\"used\":\"" $3 \
              "\",\"avail\":\"" $4 "\",\"fs\":\"" $1 "\"}"
      i++
  }
}
END {
  print "]}"
}
