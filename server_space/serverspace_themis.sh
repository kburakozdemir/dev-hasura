#!/bin/bash

# Usage
# bash /home/burak/works/gitlab/dev-hasura/server_space/serverspace.sh \
#  "GRAPHQL_API_ENDPOINT" "GRAPHQL_ADMIN_SECRET" "MACHINE" "LOCATION"

SCRIPTPATH="/root/gecici"
JSONFILESFOLDER="$SCRIPTPATH/server_space/tmp"

function insert {
  GRAPHQL_API_ENDPOINT="$1"
  GRAPHQL_ADMIN_SECRET="$2"
  MACHINE="$3"
  LOCATION="$4"

  AWKFILE="$SCRIPTPATH/server_space/server_storage_space.awk"

  SPACE=$(df -Pk | awk -v machine="$MACHINE" -v location="$LOCATION" -f $AWKFILE)

  TEMPJSON="$JSONFILESFOLDER/tmp.json"

  echo "{\"servername\": \"$MACHINE\",\"serverlocation\": \"$LOCATION\",\"serverdata\": $SPACE}" >$TEMPJSON

  $SCRIPTPATH/server_space/node_modules/.bin/gq \
    $GRAPHQL_API_ENDPOINT \
    -H "Content-Type: application/json" \
    -H "X-Hasura-Admin-Secret: $GRAPHQL_ADMIN_SECRET" \
    --queryFile="$SCRIPTPATH/server_space/serverspace.gql" \
    --variablesFile="$TEMPJSON" \
    >>$SCRIPTPATH/server_space/log.txt
}

MACHINE="$3"
LOCATION="$4"

insert $1 $2 "$MACHINE" "$LOCATION"
