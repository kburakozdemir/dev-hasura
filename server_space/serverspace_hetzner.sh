#!/bin/bash

# Usage
# bash /home/burak/works/gitlab/dev-hasura/server_space/serverspace_hetzner.sh \
#  "GRAPHQL_API_ENDPOINT" "GRAPHQL_ADMIN_SECRET"

SCRIPTPATH="/home/binbiriz/gecici"
JSONFILESFOLDER="$SCRIPTPATH/server_space/tmp"

function insertHetzner {
  GRAPHQL_API_ENDPOINT="$1"
  GRAPHQL_ADMIN_SECRET="$2"
  MACHINE="$3"
  LOCATION="$4"

  AWKFILE="$SCRIPTPATH/server_space/server_storage_space.awk"

  SPACE=$(ssh -p23 $5@$5.your-storagebox.de df -Pk | awk -v machine="$MACHINE" -v location="$LOCATION" -f $AWKFILE)

  TEMPJSON="$JSONFILESFOLDER/tmp.json"

  echo "{\"servername\": \"$MACHINE\",\"serverlocation\": \"$LOCATION\",\"serverdata\": $SPACE}" >$TEMPJSON

  $SCRIPTPATH/server_space/node_modules/.bin/gq \
    $GRAPHQL_API_ENDPOINT \
    -H "Content-Type: application/json" \
    -H "X-Hasura-Admin-Secret: $GRAPHQL_ADMIN_SECRET" \
    --queryFile="$SCRIPTPATH/server_space/serverspace.gql" \
    --variablesFile="$TEMPJSON" \
    >>$SCRIPTPATH/server_space/log.txt
}

MACHINE="BOX1"
LOCATION="Hetzner-F"

insertHetzner $1 $2 "$MACHINE" "$LOCATION" "u221870"

MACHINE="BOX2"
LOCATION="Hetzner-H"

insertHetzner $1 $2 "$MACHINE" "$LOCATION" "u313156"
