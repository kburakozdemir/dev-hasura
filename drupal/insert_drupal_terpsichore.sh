#!/bin/bash

# Usage
# bash insert.sh "GRAPHQL_API_ENDPOINT" "GRAPHQL_ADMIN_SECRET"

SCRIPTPATH="/home/binbiriz/gecici/hasura"
JSONFILESFOLDER="$SCRIPTPATH/tmp"
TIMESTAMP=$(date +"%Y-%m-%d %H:%M:%S")

function insert {
  GRAPHQL_API_ENDPOINT="$1"
  GRAPHQL_ADMIN_SECRET="$2"
  INSTANCENAME="$3"
  INSTANCEROOT="$4"

  # echo "$INSTANCEROOT""/drupal"

  cd "$INSTANCEROOT""/drupal"

  # echo "drush pm:list --format=json --fields=package,project,display_name,name,type,path,status,version"

  PMLIST=$(drush pm:list --format=json --fields=package,project,display_name,name,type,path,status,version)
  OUTDATED=$(composer show --outdated "dr*" --format=json)
  SECURITY=$(drush sec --format=json)

  echo $PMLIST >"$JSONFILESFOLDER/$INSTANCENAME""_""PMLIST.json"
  echo $OUTDATED >"$JSONFILESFOLDER/$INSTANCENAME""_""OUTDATED.json"
  echo $SECURITY >"$JSONFILESFOLDER/$INSTANCENAME""_""SECURITY.json"

  if [[ $PMLIST == "[]" || $PMLIST == "" ]]; then
    PMLIST="null"
  fi

  if [[ $OUTDATED == "[]" || $OUTDATED == "" ]]; then
    OUTDATED="null"
  fi

  if [[ $SECURITY == "[]" || $SECURITY == "" ]]; then
    SECURITY="null"
  fi

  TEMPJSON="$JSONFILESFOLDER/tmp.json"

  echo "{\"server\": \"terpsichore\",\"servertime\": \"$5\",\"instancename\": \"$INSTANCENAME\",\"instancedatapmlist\": $PMLIST,\"instancedataoutdated\": $OUTDATED,\"instancedatasecurity\": $SECURITY}" >$TEMPJSON

  $SCRIPTPATH/node_modules/.bin/gq \
    $GRAPHQL_API_ENDPOINT \
    -H "Content-Type: application/json" \
    -H "X-Hasura-Admin-Secret: $GRAPHQL_ADMIN_SECRET" \
    --queryFile="$SCRIPTPATH/drupal.gql" \
    --variablesFile="$TEMPJSON" \
    >>$SCRIPTPATH/log.txt
}

drupalinstance=(
  "test-medmis"
  "test-studentally"
  "test-wald"
  "test-wodo"
  "test-yediagac"
)

drupalinstanceroot=(
  "/var/www/vhosts/binbiriz.com/test-medmis.binbiriz.com"
  "/var/www/vhosts/binbiriz.com/test-studentally.binbiriz.com"
  "/var/www/vhosts/binbiriz.com/test-wald.binbiriz.com"
  "/var/www/vhosts/binbiriz.com/test-wodo.binbiriz.com"
  "/var/www/vhosts/binbiriz.com/test-yediagac.binbiriz.com"
)

for i in ${!drupalinstance[@]}; do
  DINSTANCE=${drupalinstance[$i]}
  DINSTANCEROOT=${drupalinstanceroot[$i]}
  echo "drupalinstance = " $DINSTANCE
  # echo "drupalinstanceroot = " $DINSTANCEROOT
  # echo "cmd is insert $1 $2 \"$DINSTANCE\" \"$DINSTANCEROOT\" \"$TIMESTAMP\""
  insert $1 $2 "$DINSTANCE" "$DINSTANCEROOT" "$TIMESTAMP"
done
