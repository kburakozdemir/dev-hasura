#!/bin/bash

declare -a libraries

libraries[0]='1;vuejs/vue;vue;js'
libraries[2]='2;MithrilJS/mithril.js;mithril;js'
libraries[1]='3;sveltejs/svelte;svelte;js'
libraries[3]='4;facebook/react;react;js'
libraries[4]='5;angular/angular;angular;js'
libraries[5]='6;infernojs/inferno;inferno;js'
libraries[6]='7;emberjs/ember.js;ember;js'
libraries[7]='8;koajs/koa;koa;js'
libraries[8]='9;hapijs/hapi;hapi;js'
libraries[9]='10;nestjs/nest;nest;js'
libraries[10]='11;marko-js/marko;marko;js'
libraries[11]='12;Bobris/Bobril;Bobril;js'
libraries[12]='13;redom/redom;redom;js'
libraries[13]='14;adamhaile/surplus;surplus;js'
libraries[14]='15;yelouafi/petit-dom;petit-dom;js'
libraries[15]='16;vuejs/petite-vue;petite-vue;js'
libraries[16]='17;alpinejs/alpine;alpine;js'
libraries[17]='18;clue/framework-x;Framework X;php'
libraries[18]='19;symfony/symfony;The Symfony PHP framework;php'
libraries[19]='20;laravel/framework;The Laravel Framework;php'
libraries[20]='21;slimphp/Slim;Slim;php'
libraries[21]='22;jgthms/bulma;Bulma;css'
libraries[22]='23;twbs/bootstrap;Bootstrap;css'
libraries[23]='24;tailwindlabs/tailwindcss;Tailwind CSS;css'
libraries[24]='25;jquery/jquery;jQuery JavaScript Library;css'

